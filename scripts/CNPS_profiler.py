#!/usr/bin/python3

## Generate count tables (raw, RPK and TPM-normalised) of pre-processed reads that mapped to annotated CAZy, NCyc, PCyc or SCyc protein-coding genes
## In the result tables, gene_family is referred to as "level_1" and gene_id as "level_2" (downstream compatibility reasons)
## Input: CAZy, NCyc, PCyc or SCyc alignments (*.diamond) and corresponding mapping (accession_number-to-gene_id-to-gene_family)
## Note: E-values are currently not parsed from alignment data but could be incorporated to customise different cutoffs

## Changelog (mini)
## 20241005, rename RPK times longest ORF variable and output files
## 20240910, multiply RPK with longest ORF (per sample)
## 20231024, Re-introdce RPK calculations; rename script (previously: nitrocarb_profiler.py)
## 20230905, Extend annotation option p for PCyc
## 20230904, Extend annotation option s for SCyc
## 20230623, Merge columns accession and level_2 to avoid non-unique row entries (CAZy GenBank accessions can have double assignments)
## 20230622, Merge gene2level mapping with accession_number-to-gene_id mapping (rid one argument/input file)
## 20230608, Add gene2level mapping file (gene_id-to-gene_family)
## 20230220, Make help, usage and version parsing more robust
## 20230217, Code cleanup: include versions; output raw count and TPM table
## 20220929, NCyc/CAZy profiler, initial version

versionSelf = '2024.10.05'

import os
import sys
import glob
import re
import numpy as np
#import statistics
try:
    import pandas as pd
except ImportError:
    subprocess.check_call([sys.executable, "-m", "pip", "install", 'pandas'])
finally:
    import pandas as pd

## Version and argument handling
if len(sys.argv) != 4 or sys.argv[1] == 'help':
    if len(sys.argv) == 2 and sys.argv[1] == 'version':
        print ('\n' + os.path.basename(__file__) + ':\t' + versionSelf + '\n')
        print ('Python:\t' + sys.version)
        print ('numpy:\t' + np.__version__)
        print ('pandas:\t' + pd.__version__)
        print ('re:\t' + re.__version__)
        quit()
    print("""\

  CNPS_profiler = C, N, P or S metabolism profiling

  Example:\t./CNPS_profiler.py n "*.diamond" NCyc_map_20190728.tsv

  \t\t... c for CAZy, n for NCyc, p for PCyc and s for SCyc data
  \t\t... DIAMOND alignment(s) of CAZy, NCyc, PCyc or SCyc proteins, default BLAST output (quote if wildcards are used)
  \t\t... CAZy, NCyc, PCyc or SCyc accession_number-to-gene_id-to-gene_family mapping file
""")
    sys.exit(1)

## Parse arguments
annotation = sys.argv[1]
if annotation == 'c':
    annotation = 'CAZy'
elif annotation == 'n':
    annotation = 'NCyc'
elif annotation == 'p':
    annotation = 'PCyc'
elif annotation == 's':
    annotation = 'SCyc'
else:
    print("unknown annotation data:", annotation)
    sys.exit(1)

alignData = sorted(glob.glob(sys.argv[2]))
sampleNames = list(map(lambda st: str.replace(st, ".diamond", ""), alignData))

mapFile = sys.argv[3]

## Parse mapping file directly into dictionary and reorder columns
print("... parsing", mapFile)
allMap = pd.read_csv(mapFile, sep='\t', header=None, usecols=[0, 1, 2], names=['accession', 'level_2', 'level_1'])
allMap = allMap[['accession', 'level_1', 'level_2']]

## Prepare accession2gene lookup table for later (read source again seems faster than converting from df!)
print("... prepare acc2gene lookup table")
geneMap = pd.read_csv(mapFile, sep='\t', header=None, index_col=0, usecols=[0,1]).to_dict()[1]

## Prepare result tables (raw counts and TPM)
resTableMap = allMap.drop_duplicates(subset=['level_2'], keep='first').sort_values(by='level_2', ignore_index=True)
resTableRPK = resTableMap
resTableRPKmaxORF = resTableMap
resTableTPM = resTableMap

## Process each alignment file
for sampleName in sampleNames:
    print("... processing", sampleName)
    sampleFile = sampleName + '.diamond'
    colNames = ['contig', 'accession']
    ## Note: Pandas 1.5.0 can also read tar.gz files with compression='infer'
    contig2acc = pd.read_csv(sampleFile, sep='\t', header=None, index_col=0, usecols=[0,1], names=colNames)

    ## Look up gene_id for accession_number in mapping file; drop rows if gene=NaN
    contig2acc['level_2'] = contig2acc['accession'].map(geneMap)
    contig2acc = contig2acc.dropna(subset=['level_2'])

    ## DEBUG
    #print(contig2acc)
    #print(contig2acc.info(verbose=True))

    ## Expand fasta header and extract mapping stats
    contig2acc['fastaHeader'] = contig2acc.index.str.split('|').str[-1].astype('str')
    contig2acc['ORF_length'] = contig2acc['fastaHeader'].str.split('_').str[0].astype('int')
    contig2acc['mapped'] = contig2acc['fastaHeader'].str.split('_').str[1].astype('int')
    contig2acc['mappedTrue'] = contig2acc['fastaHeader'].str.split('_').str[2].astype('int')
    contig2acc['meandepth'] = contig2acc['fastaHeader'].str.split('_').str[3].astype('float')
    contig2acc = contig2acc.reset_index(drop=True)

    ## Prepare and fill stats dataframe
    gene2stats = pd.DataFrame()
    gene2stats[sampleName + '_' + 'map_sum'] = contig2acc.groupby('level_2', sort=False)['mapped'].sum()
    gene2stats[sampleName + '_' + 'ORF_bp_mean'] = contig2acc.groupby('level_2', sort=False)['ORF_length'].mean()
    gene2stats[sampleName + '_' + 'map_sum_all'] = contig2acc.groupby('level_2', sort=False)['mappedTrue'].sum()
    gene2stats[sampleName + '_' + 'sum_depth'] = contig2acc.groupby('level_2', sort=False)['meandepth'].sum()
    gene2stats[sampleName + '_' + 'RPK'] = gene2stats[sampleName + '_' + 'map_sum'] / (gene2stats[sampleName + '_' + 'ORF_bp_mean'] * 0.001)
    ORF_bp_max = contig2acc['ORF_length'].max()
    gene2stats[sampleName + '_' + 'RPKmaxORF'] = (gene2stats[sampleName + '_' + 'RPK'] * ORF_bp_max * 0.001).round(0)
    sumRPK = gene2stats[sampleName + '_' + 'RPK'].sum()

    ## DEBUG
    #print(contig2acc.dtypes)
    #print(contig2acc.to_string(max_rows=10))
    #print(contig2acc.info(verbose=True))
    #print(gene2stats)
    #print(sampleName + " sum RPK: " + sumRPK.astype('str'))

    ## Prepare result tables (raw counts and calculate TPM based on RPK)
    resTableMap = resTableMap.merge(gene2stats[[sampleName + '_' + 'map_sum']], on='level_2', how='left', sort=True).drop_duplicates(keep='first')
    resTableRPK = resTableRPK.merge(gene2stats[[sampleName + '_' + 'RPK']], on='level_2', how='left', sort=True).drop_duplicates(keep='first')
    resTableRPKmaxORF = resTableRPKmaxORF.merge(gene2stats[[sampleName + '_' + 'RPKmaxORF']], on='level_2', how='left', sort=True).drop_duplicates(keep='first')
    gene2stats[sampleName + '_' + 'TPM'] = (gene2stats[sampleName + '_' + 'RPK'] / (sumRPK / 1000000))
    resTableTPM = resTableTPM.merge(gene2stats[[sampleName + '_' + 'TPM']], on='level_2', how='left', sort=True).drop_duplicates(keep='first')

## Remove temporary sample name suffices
resTableMap.columns = resTableMap.columns.map(lambda x: x.removesuffix('_map_sum'))
resTableRPK.columns = resTableRPK.columns.map(lambda x: x.removesuffix('_RPK'))
resTableRPKmaxORF.columns = resTableRPKmaxORF.columns.map(lambda x: x.removesuffix('_RPKmaxORF'))
resTableTPM.columns = resTableTPM.columns.map(lambda x: x.removesuffix('_TPM'))
## Merge accession with level_2 column (workaround for CAZy GenBank accession with double-assignments; keep entries unique)
resTableMap['accession'] = resTableMap['accession'] + '_' + resTableMap['level_2']
resTableRPK['accession'] = resTableRPK['accession'] + '_' + resTableRPK['level_2']
resTableRPKmaxORF['accession'] = resTableRPKmaxORF['accession'] + '_' + resTableRPKmaxORF['level_2']
resTableTPM['accession'] = resTableTPM['accession'] + '_' + resTableTPM['level_2']


## Write results, na_rep: missing data representation (NA)
print("... write result tables")
resTableMap.to_csv(annotation + '_count.tsv', sep='\t', na_rep='NA', header=True, index=False)
resTableRPK.to_csv(annotation + '_RPK.tsv', sep='\t', na_rep='NA', header=True, index=False)
resTableRPKmaxORF.to_csv(annotation + '_RPKmaxORF.tsv', sep='\t', na_rep='NA', header=True, index=False)
resTableTPM.to_csv(annotation + '_TPM.tsv', sep='\t', na_rep='NA', header=True, index=False)

print("done.")
