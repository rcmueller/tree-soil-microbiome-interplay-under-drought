#!/usr/bin/python3

## Generate result tables for functional annotation and mappings
## Argument: functional annotation category (EC|EGGNOG|INTERPRO2GO|SEED)
## Input files for all samples (sampleName) and each category (funcAnnot):
## - sampleName_funcAnnot_acc_to_path.tsv                accession to path
## Output: (raw) count, RPK, RPK multiplied with longest ORF and TPM tables with accession number and path (ontology)

## Changelog (mini)
## 20241005, rename RPK times longest ORF variable and output files
## 20240604, multiply RPK with longest ORF (per sample)
## 20231024, re-introduce RPK table output
## 20230706, strip down to count and TPM tables only; get rid of sample name suffixes
## 20230228, make regex in seqkit stats file parsing more distinct/robust
## 20230228, drop rows with mappedTrue=0 (correct ORF_bp_mean calculation: only consider ORFs with mappings; affects tables ORF_bp_mean, RPK, FPKM, TPM)
## 20230220, make help, usage, version parsing more robust
## 20230220, code clean-up, added versioning
## 20230220, allow sample name without .gz suffix in seqstats.txt
## 20230119, corrected error (drop_duplicates on mapStats: does not consider index=accession)
## 20220509, generate result tables for c2c, mean ORF, mapped, all mapped, meandepth, RPK, FPKM, TPM
## 20220504, re-write for alternative pipeline: mapping-annotation
## 20220111, muellerra, f(unctional)annot(ation)_res(ult)gen(eration)

versionSelf = '2024.10.05'

import os
import sys
import glob
import re
import numpy as np
#import statistics
try:
    import pandas as pd
except ImportError:
    subprocess.check_call([sys.executable, "-m", "pip", "install", 'pandas'])
finally:
    import pandas as pd

## Version and argument handling
if len(sys.argv) != 2 or sys.argv[1] == 'help' or sys.argv[1] == 'version':
    if len(sys.argv) == 2 and sys.argv[1] == 'version':
        print ('\n' + os.path.basename(__file__) + ':\t' + versionSelf + '\n')
        print ('Python:\t' + sys.version)
        print ('Numpy:\t' + np.__version__)
        print ('Pandas:\t' + pd.__version__)
        print ('Re:\t' + re.__version__ + '\n')
        quit()
    print("""\

  fannot_resgen = f(unctional) annot(ation) res(ult tables) gen(eration):
  Create raw count and TPM tables based on MEGAN annotations

  Example:\t./fannot_resgen.py ec

  \t\t... annotation_category: EC, EGGNOG, INTERPRO2GO, SEED (case insensitive)
  \t\t... required files:      *_acc_to_path.tsv

  Versions:\t./fannot_resgen.py version
""")
    sys.exit(1)

funcAnnot = sys.argv[1].upper()
startDir = os.getcwd() 
workDir = startDir + '/' + funcAnnot
try:
    os.chdir(workDir)
except:
    print("Cannot change to " + workDir)
    sys.exit(1)

## Prepare sample list (based on accession-to-path files)
print(funcAnnot + ": Prepare sample list")

sampleA2Plist = sorted(glob.glob(workDir + '/*_' + funcAnnot + '_acc_to_path.tsv'))
sampleNames = []
for filePath in sampleA2Plist:
    splitPath = re.split(r'/|_', os.path.basename(filePath))
    # optional conditional: all needed files for this sample are available or throw warning/error+exit
    sampleNames.append(splitPath[0]) # tweak to extract e.g., "134-5-10-2020" in file "134-5-10-2020_EC_acc_to_path.tsv"

## Get all unique accession-path values from extracted acc_to_path files, accession as index, drop duplicates, expand path column, drop first and last column
print(funcAnnot + ": Read acc_to_path files")
resTable = pd.concat((pd.read_csv(sampleA2P, sep='\t', header=None, usecols=[1,2], index_col=0) for sampleA2P in sampleA2Plist)).drop_duplicates(keep='first')
resTable = resTable[2].str.split(';', expand=True).iloc[:,1:-1] # omit first (category name) and last column (dispensable ';' produced by MEGAN export at EOL)
resTable = resTable.replace(r'^$', np.nan, regex=True) # Replace blank fields with NA
resTable.index.name = 'accession' # Set name for index column

## Create header dynamically based on number of level columns in annotation category
print(funcAnnot + ": Prepare result tables")
levelHead = []
for level in range(resTable.shape[1]):
    levelHead.append("level_" + str(level + 1))
resTable.columns = levelHead

## Prepare result tables (raw counts of mapped reads, RPKs, RPK times longest ORF and TPMs)
resTableMap = resTable
resTableRPK = resTable
resTableRPKmaxORF = resTable
resTableTPM = resTable

print(funcAnnot + ": Calculate sample stats")
for sampleName in sampleNames:
    ## Parse mapping stats, set accession as index and left join by accession
    print(funcAnnot + ", " + sampleName + ": Processing mapping stats")
    mapFile = workDir + '/' + sampleName + '_' + funcAnnot + '_acc_to_path.tsv'
    colNames = ['fastaHeader', 'accession']
    mapStats = pd.read_csv(mapFile, sep='\t', header=None, index_col=1, usecols=[0, 1], names=colNames)
    mapStats.index = mapStats.index.astype('int64') # Convert index to int64 for later join to work

    ## Extract mapping stats from fastaHeader (drop contig name, separators=|_)
    mapStats['fastaHeader'] = mapStats['fastaHeader'].str.split('|').str[-1].astype('str')
    mapStats['ORF_length'] = mapStats['fastaHeader'].str.split('_').str[0].astype('int')
    mapStats['mapped'] = mapStats['fastaHeader'].str.split('_').str[1].astype('int')
    mapStats['mappedTrue'] = mapStats['fastaHeader'].str.split('_').str[2].astype('int')
    mapStats['meandepth'] = mapStats['fastaHeader'].str.split('_').str[3].astype('float')

    ## Drop rows with mappedTrue=0 (correct ORF_bp_mean calculation: only consider ORFs with mappings; affects tables ORF_bp_mean, RPK, FPKM, TPM)
    mapStats = mapStats[mapStats.mappedTrue != 0]

    ## Calculate (per accession) mean ORF length, max ORF length, sum mapped, RPK, RPK times longest ORF; extract total RPK sum; drop ORF_length and mapped, drop duplicates
    mapStats[sampleName + '_' + 'map_sum'] = mapStats.groupby('accession', sort=False)['mapped'].sum()
    mapStats[sampleName + '_' + 'ORF_bp_mean'] = mapStats.groupby('accession', sort=False)['ORF_length'].mean()
    mapStats[sampleName + '_' + 'map_sum_all'] = mapStats.groupby('accession', sort=False)['mappedTrue'].sum()
    mapStats[sampleName + '_' + 'sum_depth'] = mapStats.groupby('accession', sort=False)['meandepth'].sum()
    mapStats[sampleName + '_' + 'RPK'] = mapStats[sampleName + '_' + 'map_sum'] / (mapStats[sampleName + '_' + 'ORF_bp_mean'] * 0.001)
    ORF_bp_max = mapStats['ORF_length'].max()
    mapStats[sampleName + '_' + 'RPKmaxORF'] = (mapStats[sampleName + '_' + 'RPK'] * ORF_bp_max * 0.001).round(0)
    mapStats.drop(['fastaHeader', 'ORF_length', 'mapped', 'mappedTrue', 'meandepth'], axis=1, inplace=True)
    #mapStats.drop_duplicates(inplace=True)
    mapStats = mapStats[~mapStats.index.duplicated()]
    sumRPK = mapStats[sampleName + '_' + 'RPK'].sum()
    #print(funcAnnot + ", " + sampleName + " sum RPK: " + sumRPK.astype('str'))

    ## Calculate TPM based on RPK
    mapStats[sampleName + '_' + 'TPM'] = (mapStats[sampleName + '_' + 'RPK'] / (sumRPK / 1000000))

    ## Left join mapping stats with result table based on accession, drop duplicates
    resTableMap = resTableMap.merge(mapStats[[sampleName + '_' + 'map_sum']], on='accession', how='left', sort=True).drop_duplicates(keep='first')
    resTableRPK = resTableRPK.merge(mapStats[[sampleName + '_' + 'RPK']], on='accession', how='left', sort=True).drop_duplicates(keep='first')
    resTableRPKmaxORF = resTableRPKmaxORF.merge(mapStats[[sampleName + '_' + 'RPKmaxORF']], on='accession', how='left', sort=True).drop_duplicates(keep='first')
    resTableTPM = resTableTPM.merge(mapStats[[sampleName + '_' + 'TPM']], on='accession', how='left', sort=True).drop_duplicates(keep='first')

    ##### debug #####
    ## print(mapStats)
    ## print(mapStats.index.dtype)

## Remove temporary sample name suffices
resTableMap.columns = resTableMap.columns.map(lambda x: x.removesuffix('_map_sum'))
resTableTPM.columns = resTableTPM.columns.map(lambda x: x.removesuffix('_TPM'))
resTableRPK.columns = resTableRPK.columns.map(lambda x: x.removesuffix('_RPK'))
resTableRPKmaxORF.columns = resTableRPKmaxORF.columns.map(lambda x: x.removesuffix('_RPKmaxORF'))

## Write results, na_rep: missing data representation (NA)
print(funcAnnot + ": Write result tables to " + startDir)
os.chdir(startDir)
resTableMap.to_csv(funcAnnot + '_count.tsv', sep='\t', na_rep='NA', header=True)
resTableTPM.to_csv(funcAnnot + '_TPM.tsv', sep='\t', na_rep='NA', header=True)
resTableRPK.to_csv(funcAnnot + '_RPK.tsv', sep='\t', na_rep='NA', header=True)
resTableRPKmaxORF.to_csv(funcAnnot + '_RPKmaxORF.tsv', sep='\t', na_rep='NA', header=True)

print(funcAnnot + ": Done.")
