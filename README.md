## When trees die: Understanding how plants and microbes interact and influence soil biogeochemical processes

This repository contains detailed description of the metagenome data processing pipeline in the [Wiki](https://gitlab.com/rcmueller/tree-soil-microbiome-interplay-under-drought/-/wikis/home) including links and references to external software, and tailor-made scripts and resources which can be downloaded here.
